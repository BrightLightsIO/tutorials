﻿using UnityEngine;
using System.Collections.Generic;

namespace BrightLights
{
    public class DynamicMesh : MonoBehaviour
    {
        private const float ANGLE_STEP = 10.0f;

        private readonly Vector3 INNER_RADIUS = new Vector3(0.5f, 0.0f, 0.0f);
        private readonly Vector3 OUTER_RADIUS = new Vector3(1.0f, 0.0f, 0.0f);

        private Mesh mesh;

        private MeshFilter meshFilter;

        private MeshRenderer meshRenderer;

        void Start()
        {
            CreateMesh();

            SetupComponents();
        }

        void Update()
        {

        }

        private void SetupComponents()
        {
            // Find or create the required components
            meshFilter = gameObject.GetComponent<MeshFilter>();
            if (meshFilter == null)
            {
                meshFilter = gameObject.AddComponent<MeshFilter>();
            }

            meshRenderer = gameObject.GetComponent<MeshRenderer>();
            if (meshRenderer == null)
            {
                meshRenderer = gameObject.AddComponent<MeshRenderer>();
            }

            // Apply mesh
            meshFilter.sharedMesh = mesh;
        }

        private void CreateMesh()
        {
            mesh = new Mesh();

            Vector3[] vertices = GenerateVertices();

            mesh.vertices = vertices;

            mesh.uv = GenerateUVs(vertices.Length);

            mesh.triangles = GenerateTriangles(vertices.Length);

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        private Vector3[] GenerateVertices()
        {
            List<Vector3> vertices = new List<Vector3>();

            Quaternion rotation = Quaternion.identity;

            for (float angle = 0; angle <= 360.0f; angle += ANGLE_STEP)
            {
                rotation.eulerAngles = new Vector3(0, 0, angle);

                vertices.Add(rotation * INNER_RADIUS);
                vertices.Add(rotation * OUTER_RADIUS);
            }

            return vertices.ToArray();
        }

        private int[] GenerateTriangles(int numVertices)
        {
            List<int> triangles = new List<int>();

            bool even = true;

            // Generate (numVertices - 2) triangles
            for (int i = 2; i < numVertices; i++)
            {
                // Maintain triangle winding so all triangles
                // are facing the same direction.
                if (even)
                {
                    triangles.Add(i);
                    triangles.Add(i - 1);
                    triangles.Add(i - 2);
                }
                else
                {
                    triangles.Add(i - 2);
                    triangles.Add(i - 1);
                    triangles.Add(i);
                }

                even = !even;
            }

            return triangles.ToArray();
        }

        private Vector2[] GenerateUVs(int numVertices)
        {
            List<Vector2> uvs = new List<Vector2>();

            for (int i = 0; i < numVertices; i += 2)
            {
                uvs.Add(new Vector2(i / 2, 0));
                uvs.Add(new Vector2(i / 2, 1));
            }

            return uvs.ToArray();
        }
    }
}
