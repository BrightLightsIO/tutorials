﻿using UnityEngine;
using System.Collections.Generic;

namespace BrightLights
{
    public class UpdatingDynamicMesh : MonoBehaviour
    {
        private const float ANGLE_STEP = 10.0f;

        private readonly Vector3 INNER_RADIUS = new Vector3(0.5f, 0.0f, 0.0f);
        private readonly Vector3 OUTER_RADIUS = new Vector3(1.0f, 0.0f, 0.0f);

        private const float MIN_SCALE = 1.0f;
        private const float MAX_SCALE = 2.0f;

        private Mesh mesh;

        private float scale;

        private float scaleDir;

        void Start()
        {
            scale = MIN_SCALE;
            scaleDir = 1.0f;
        }

        void Update()
        {
            UpdateScale();

            RegenerateMesh();
        }

        private void UpdateScale()
        {
            scale += MAX_SCALE * Time.smoothDeltaTime * scaleDir;

            if (scale > MAX_SCALE)
            {
                scale = MAX_SCALE;
                scaleDir = -1.0f;
            }
            else if (scale <= 1.0f)
            {
                scale = MIN_SCALE;
                scaleDir = 1.0f;
            }
        }

        private void RegenerateMesh()
        {
            // Find the mesh
            if (mesh == null)
            {
                MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
                if (meshFilter != null)
                {
                    mesh = meshFilter.sharedMesh;
                }
            }

            if (mesh != null)
            {
                // @B - Mesh Clear if modifying triangles and vertices
                Vector3[] vertices = GenerateVertices();

                mesh.vertices = vertices;

                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
            }
        }

        private Vector3[] GenerateVertices()
        {
            List<Vector3> vertices = new List<Vector3>();

            Quaternion rotation = Quaternion.identity;

            for (float angle = 0; angle <= 360.0f; angle += ANGLE_STEP)
            {
                rotation.eulerAngles = new Vector3(0, 0, angle);

                vertices.Add(rotation * INNER_RADIUS);
                vertices.Add(rotation * (scale * OUTER_RADIUS));
            }

            return vertices.ToArray();
        }
    }
}
