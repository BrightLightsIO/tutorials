﻿using UnityEngine;
using System.Collections.Generic;

namespace BrightLights
{
    public class ProceduralMesh : MonoBehaviour
    {
        private const float ANGLE_STEP = 10.0f;

        private readonly Vector3 INNER_RADIUS = new Vector3(0.5f, 0.0f, 0.0f);
        private readonly Vector3 OUTER_RADIUS = new Vector3(1.0f, 0.0f, 0.0f);

        public Material[] Materials;

        private Mesh mesh;

        private MeshFilter meshFilter;

        private MeshRenderer meshRenderer;

        void Start()
        {
            CreateMesh();

            SetupComponents();
        }

        void Update()
        {

        }

        private void SetupComponents()
        {
            // Find or create the required components
            meshFilter = gameObject.GetComponent<MeshFilter>();
            if (meshFilter == null)
            {
                meshFilter = gameObject.AddComponent<MeshFilter>();
            }

            meshRenderer = gameObject.GetComponent<MeshRenderer>();
            if (meshRenderer == null)
            {
                meshRenderer = gameObject.AddComponent<MeshRenderer>();
            }

            // Set Materials
            // Each material will be assigned to the corrisponding submesh
            meshRenderer.sharedMaterials = Materials;

            // Apply mesh
            meshFilter.sharedMesh = mesh;
        }

        private void CreateMesh()
        {
            mesh = new Mesh();

            Vector3[] vertices = GenerateVertices();

            mesh.vertices = vertices;

            mesh.uv = GenerateUVs(vertices.Length);

            GenerateTriangles(vertices.Length);

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        private Vector3[] GenerateVertices()
        {
            List<Vector3> vertices = new List<Vector3>();

            Quaternion rotation = Quaternion.identity;

            // Ensure we have an equivalent number of vertices
            // for each submesh
            int segments = (int)(360.0f / ANGLE_STEP);
            int remainder = segments % Materials.Length;
            if (remainder > 0)
            {
                segments += Materials.Length - remainder;
            }

            float angleStep = 360.0f / segments;

            for (float angle = 0; angle <= 360.0f; angle += angleStep)
            {
                rotation.eulerAngles = new Vector3(0, 0, angle);

                vertices.Add(rotation * INNER_RADIUS);
                vertices.Add(rotation * OUTER_RADIUS);
            }

            return vertices.ToArray();
        }

        private void GenerateTriangles(int numVertices)
        {
            List<int> triangles = new List<int>();

            bool even = true;

            // One submesh per Material provided
            int numSubmeshes = Materials.Length;
            int numVerticesPerSegment = numVertices / numSubmeshes;

            // Set the number of submeshes the mesh should expect
            mesh.subMeshCount = numSubmeshes;

            for (int submesh = 0; submesh < numSubmeshes; submesh++)
            {
                triangles.Clear();
        
                // Iterate over the submeshes vertices creating triangles.
                // Submeshes can share vertices.
                int startVertex = (submesh * numVerticesPerSegment) + 2;
                int endVertex = startVertex + numVerticesPerSegment;

                // Generate (numVertices - 2) triangles for each submesh
                for (int i = startVertex; i < endVertex; i++)
                {
                    // Maintain triangle winding so all triangles
                    // are facing the same direction.
                    if (even)
                    {
                        triangles.Add(i);
                        triangles.Add(i - 1);
                        triangles.Add(i - 2);
                    }
                    else
                    {
                        triangles.Add(i - 2);
                        triangles.Add(i - 1);
                        triangles.Add(i);
                    }

                    even = !even;
                }

                // Set the indices for the submesh.
                // The indices should be interpreted as Triangles.
                mesh.SetIndices(triangles.ToArray(), MeshTopology.Triangles, submesh);
            }
        }

        private Vector2[] GenerateUVs(int numVertices)
        {
            List<Vector2> uvs = new List<Vector2>();

            for (int i = 0; i < numVertices; i += 2)
            {
                uvs.Add(new Vector2(i / 2, 0));
                uvs.Add(new Vector2(i / 2, 1));
            }

            return uvs.ToArray();
        }
    }
}
