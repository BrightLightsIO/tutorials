﻿using UnityEngine;
using System.Collections.Generic;

namespace BrightLights
{
    public class Physics3DMesh : MonoBehaviour
    {
        private MeshCollider meshCollider;

        void Start()
        {
        }

        void Update()
        {
            SetupPhysics();
        }

        private void SetupPhysics()
        {
            // If a MeshCollider has not already been created
            if (meshCollider == null)
            {
                // Find the mesh using the MeshFilter
                MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
                if (meshFilter)
                {
                    Mesh mesh = meshFilter.sharedMesh;

                    bool hasRigidBody = gameObject.GetComponent<Rigidbody>() != null;

                    // Find or create the MeshCollider
                    meshCollider = gameObject.GetComponent<MeshCollider>();
                    if (meshCollider == null)
                    {
                        meshCollider = gameObject.AddComponent<MeshCollider>();
                    }

                    meshCollider.sharedMesh = mesh;

                    // If the GameObject has a RigidBody, the mesh collider must be
                    // flagged as convex.
                    // The mesh must also be convex; it cannot have holes or dents.
                    if (hasRigidBody)
                    {
                        meshCollider.convex = true;
                    }
                }
            }
        }
    }
}
